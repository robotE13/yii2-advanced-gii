<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\gii\module;

use yii\gii\CodeFile;
use yii\helpers\StringHelper;

/**
 * Description of Generator
 *
 * @author Tartharia
 */
class Generator extends \yii\gii\generators\module\Generator
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Module Generator. Advanced template';
    }

        /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator helps generate the skeleton code needed by a Yii module. Template with separate controllers between back/front';
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];
        $modulePath = $this->getModulePath();
        $files[] = new CodeFile(
            $modulePath . '/' . StringHelper::basename($this->moduleClass) . '.php',
            $this->render("module.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/frontend/controllers/MainController.php',
            $this->render("controller.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/frontend/views/main/index.php',
            $this->render("view.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/backend/controllers/MainController.php',
            $this->render("controller.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/backend/views/main/index.php',
            $this->render("view.php")
        );
        $files[] = new CodeFile(
            $modulePath . "/messages/ru/{$this->moduleID}.php",
            $this->render("translation.php")
        );

        return $files;
    }

    /**
     * @return string the controller namespace of the module.
     */
    public function getControllerNamespace()
    {
        return substr($this->moduleClass, 0, strrpos($this->moduleClass, '\\')) . '\frontend\controllers';
    }
}
