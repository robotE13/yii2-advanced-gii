<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$("document").ready(function(){
        $("#new-record").on("pjax:end", function() {
            $.pjax.reload({container:"#view-index"});  //Reload GridView
        });
    });'
);
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">
    <?="<?php "?>Pjax::begin(['id'=>'new-record']);<?=" ?>\n"?>
        <?= "<?php " ?>$form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "\t\t\t<?= " . $generator->generateActiveField($attribute) . " ?>\n";
    }
} ?>
        
            <div class="form-group">
                <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Create') ?> : <?= $generator->generateString('Update') ?>, ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        <?= "<?php " ?>ActiveForm::end();?>
    <?="<?php "?>Pjax::end();<?="?>\n"?>
</div>
