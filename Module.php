<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\gii;

use yii\helpers\ArrayHelper;

/**
 * Description of Module
 *
 * @author Tartharia
 */
class Module extends \yii\gii\Module{

    public function init() {
        parent::init();
        $coreGenerators = $this->coreGenerators();
        $this->generators['crud'] = ArrayHelper::merge($coreGenerators['crud'],[
            'templates'=>[
                'pjax'=>'@robote13/gii/crud/pjax',
                'withAbstractCrud'=>'@robote13/gii/crud/crud',
                'default'=>'@robote13/gii/crud/default',
            ]
        ]);
        $this->setViewPath('@yii/gii/views');
    }

    protected function coreGenerators() {
        $generators = parent::coreGenerators();
        $generators['adv-module']=[ // generator name
            'class' => 'robote13\gii\module\Generator', // generator class
            'templates' => [ //setting for out templates
                'default' => '@robote13/gii/module/default', // template name => path to template
            ]
        ];
        return $generators;
    }
}
